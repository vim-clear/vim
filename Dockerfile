FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > vim.log'

COPY vim .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' vim
RUN bash ./docker.sh

RUN rm --force --recursive vim
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD vim
